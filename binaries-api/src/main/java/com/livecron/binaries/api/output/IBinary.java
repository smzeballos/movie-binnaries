package com.livecron.binaries.api.output;

/**
 * @author Santiago Mamani
 */
public interface IBinary {

    String getId();

    String getMimeType();

    String getName();

    Long getSize();
}
