package com.livecron.binaries.service.service;

import com.livecron.binaries.service.model.domain.Binary;
import org.springframework.stereotype.Service;

/**
 * @author Santiago Mamani
 */
@Service
public class BinaryCreateService extends AbstractBinaryService {

    @Override
    protected Binary loadBinaryInstance() {
        return new Binary();
    }
}
