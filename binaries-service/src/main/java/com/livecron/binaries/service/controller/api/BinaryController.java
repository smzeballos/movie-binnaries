package com.livecron.binaries.service.controller.api;

import com.livecron.binaries.service.service.BinaryContentReadService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

/**
 * @author Santiago Mamani
 */
@RequestMapping(value = "/binaries")
@RestController
@RequestScope
public class BinaryController {

    @Autowired
    private BinaryContentReadService binaryContentReadService;

    @ApiOperation(
            value = "Download a binary content"
    )
    @RequestMapping(
            value = "{binaryId}/content",
            method = RequestMethod.GET
    )
    public ResponseEntity<InputStreamResource> download(@PathVariable("binaryId") String binaryId) {

        binaryContentReadService.setBinaryId(binaryId);
        binaryContentReadService.execute();

        InputStream content = new ByteArrayInputStream(
                binaryContentReadService.getBytes(),
                0,
                binaryContentReadService.getBytes().length);

        return ResponseEntity
                .ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "filename=" + binaryContentReadService.getBinary().getName())
                .contentType(MediaType.parseMediaType(binaryContentReadService.getBinary().getMimeType()))
                .body(new InputStreamResource(content));
    }

}
