package com.livecron.binaries.service.controller.system;

import com.livecron.binaries.api.output.IBinary;
import com.livecron.binaries.service.service.BinaryCreateService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Santiago Mamani
 */
@Api(
        tags = "binary-controller",
        description = "Operations over binaries"
)
@RequestMapping(value = "/system/binaries")
@RestController
@RequestScope
public class SystemBinaryController {

    @Autowired
    private BinaryCreateService binaryCreateService;

    @ApiOperation(
            value = "Upload a binary content"
    )
    @RequestMapping(
            method = RequestMethod.POST,
            consumes = "multipart/form-data"
    )
    public IBinary uploadBinary(@RequestPart(value = "multipartFile") MultipartFile multipartFile) {
        binaryCreateService.setFile(multipartFile);
        binaryCreateService.execute();

        return binaryCreateService.getBinary();
    }

}
