package com.livecron.binaries.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class BinariesServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(BinariesServiceApplication.class, args);
    }

}
