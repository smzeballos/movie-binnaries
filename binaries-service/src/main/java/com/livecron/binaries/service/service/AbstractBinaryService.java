package com.livecron.binaries.service.service;

import com.livecron.binaries.service.model.domain.Binary;
import com.livecron.binaries.service.model.repository.BinaryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * @author Santiago Mamani
 */
abstract class AbstractBinaryService {

    private MultipartFile file;

    private Binary binary;

    @Autowired
    private BinaryRepository repository;

    @Autowired
    private BinaryContentCreateOrUpdateService binaryContentCreateOrUpdateService;

    public void execute() {
        Binary instance = loadBinaryInstance();

        putFileInformation(instance);

        binary = repository.save(instance);

        saveFileContent();
    }

    protected abstract Binary loadBinaryInstance();

    private void putFileInformation(Binary instance) {
        instance.setName(file.getOriginalFilename());
        instance.setSize(file.getSize());
        instance.setMimeType(file.getContentType());
    }

    private void saveFileContent() {
        try {
            binaryContentCreateOrUpdateService.setBinary(binary);
            binaryContentCreateOrUpdateService.setBytes(file.getBytes());

            binaryContentCreateOrUpdateService.execute();
        } catch (IOException e) {
//            throw new BinaryCanNotReadException("Cannot read file");
        }
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    public Binary getBinary() {
        return binary;
    }

}
